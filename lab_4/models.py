from django.db import models
from django.utils import timezone

# Create your models here.

class Message(models.Model):
    name = models.CharField(max_length=99)
    email = models.EmailField()
    message = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.message
