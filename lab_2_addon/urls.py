from django.conf.urls import url
from .views import index_addon as index

urlpatterns = [
    url(r'^$', index, name='index'),
]
